from cr import p5 as cr_p5
from utils import nyi

modules = {
    1: {
        "name": "Combles réguliers",
        "prgs": {
            1: ("Rectiligne du dièdre", nyi),
            2: ("Coupe de la panne", nyi),
            3: ("Longueur de la panne", nyi),
            4: ("Herse", nyi),
            5: ("Arêtier régulier", cr_p5),
            7: ("Pavillon carré", nyi),
        },
    },
    2: {
        "name": "Combles irréguliers",
        "prgs": {
            1: ("Rectiligne du dièdre", nyi),
            2: ("Coupe de la panne", nyi),
            3: ("Longueur de la panne", nyi),
            4: ("Herses", nyi),
            5: ("Arêtier irrégulier", nyi),
            6: ("Saillies de toit", nyi),
            7: ("Combles à croupes", nyi),
        },
    },
    3: {
        "name": "Combles biais réguliers",
        "prgs": {
            0: ("Programme préliminaire", nyi),
            1: ("Rectiligne du dièdre", nyi),
            2: ("Coupe de la panne", nyi),
            3: ("Longueur de la panne", nyi),
            4: ("Herses", nyi),
            5: ("Arêtier biais régulier", nyi),
            7: ("Pavillon trapézoïdal", nyi),
        },
    },
    4: {
        "name": "Combles biais irréguliers",
        "prgs": {
            0: ("Programme préliminaire", nyi),
            1: ("Rectiligne du dièdre", nyi),
            2: ("Coupe de la panne", nyi),
            3: ("Longueur de la panne", nyi),
            4: ("Herses", nyi),
            5: ("Arêtier biais irrégulier", nyi),
            6: ("Saillies de toit", nyi),
            7: ("Combles à croupes biaises", nyi),
        },
    },
}

print("\n    >>> Charpente programmée, par Samuel Cornuault et Pablo Puerto (FC Charpente 2021) <<<   ")

str_module = "\n"
for key, val in modules.items():
    str_module += "[{}] -> {}".format(key, val["name"]) + "\n"
str_module += "Choix du module: "

error_msg = '\n --> Choisir parmi : {}'

# TODO: replace assert by IF and recursion

module = int(input(str_module))
assert module in modules.keys(), error_msg.format(list(modules.keys()))

str_prog = "\n"
for key, val in modules[module]["prgs"].items():
    str_prog += "[{}] -> {}".format(key, val[0]) + "\n"
str_prog += "Choix du programme: "

prog = int(input(str_prog))
assert prog in modules[module]["prgs"].keys(), error_msg.format(list(modules[module]["prgs"].keys()))

modules[module]["prgs"][prog][1].run()