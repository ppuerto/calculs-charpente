import numpy as np
from utils import Programme

def aretier_regulier(l, h, a, j):
    """Ce programme s'applique à l'arêtier de croupe régulière."""

    g = l * np.sqrt(2)
    ac = np.sqrt(g ** 2 + h ** 2)
    a6 = np.arctan(h / g)
    a7 = np.arctan(g / h)
    a8 = np.arctan(g / ac)
    y = j / 2 * np.sqrt(2)
    z = g - y
    at = ac / g * z
    i1 = g * np.sin(a6)
    b0 = np.arctan(g / i1)
    cd = a / (2 * np.tan(b0))

    return ac, a6, a7, a8, at, b0, cd


inputs_p5 = [
    ("L", "Reculée des versants"),
    ("H", "Hauteur du comble"),
    ("A", "Epaisseur de l'arêtier"),
    ("J", "Epaisseur du poinçon"),
]

outputs_p5 = [
    ("Ac", "Arête au couronnement", False),
    ("A6", "Angle de niveau", True),
    ("A7", "Angle d'aplomb", True),
    ("A8", "Angle sur chant brut", True),
    ("At", "Arête utile", False),
    ("B0", "Angle de délardement", True),
    ("Cd", "Cote de délardement", False),
]

p5 = Programme(aretier_regulier, inputs_p5, outputs_p5)