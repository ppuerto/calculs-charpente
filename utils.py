import numpy as np

def decdeg2dms(dd):

    mnt, sec = divmod(dd * 3600, 60)
    deg, mnt = divmod(mnt, 60)

    return deg, mnt, sec


class Programme(object):
    """docstring for Programme"""

    def __init__(self, fun, inputs, outputs):
        self.fun = fun
        self.inputs = inputs
        self.outputs = outputs

    def get_user_inputs(self):
        user_in = []
        print("\r")
        print("---> Données" + "\r")
        for a, aaa in self.inputs:
            x = input("{:<30} {} : ".format(aaa, a))
            user_in.append(float(x))
        return user_in

    def print_results(self, results):
        print("\r")
        print("---> Résultats" + "\r")

        for r, o in zip(results, self.outputs):

            if o[2]:
                deg, mnt, sec = decdeg2dms(np.degrees(r))
                r = "{:.0f}°{:.0f}'".format(deg, mnt)
            else:
                r = round(r, 2)

            print("{} = {:<8} {}".format(o[0], r, o[1]))

        print("\r")

    def run(self):
        self.print_results(self.fun(*self.get_user_inputs()))


class NotYetImplemented():
    """docstring for Programme"""

    def __init__(self):
        pass

    def get_user_inputs(self):
        user_in = []
        print("\r")
        print("---> Pas encore implémenté ..." + "\r")
        print("\r")


        return user_in

    def print_results(self):
        print("\r")
        print("---> Pas encore implémenté ..." + "\r")
        print("\r")

    def run(self):
        print("\r")
        print("---> Pas encore implémenté ..." + "\r")
        print("\r")

nyi = NotYetImplemented()