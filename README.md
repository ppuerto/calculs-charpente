# Calculs Charpente

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ppuerto%2Fcalculs-charpente/HEAD)

## Prérequis
Installer [Python3](https://www.python.org/downloads/) et [Git](https://git-scm.com/) !

## Installation

```bash
git clone https://gitlab.com/ppuerto/calculs-charpente.git
```

```bash
cd calculs-charpente
```

```bash
python3 -m venv my-venv
```

```bash
source my-venv/bin/acivate
```

```bash
pip install -r requirements.txt
```

## Lancement

```bash
source my-venv/bin/acivate
```

```bash
python run.py
```
